package baseDatos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javafxapplicacion.DetalleOrden;

public class PruebaConexion {

    static Connection cn;
    static PreparedStatement ps;
    static ResultSet rs;
    static String query;
    static boolean existe;
    static int orderID;

    public static void main(String args[]) {

        ArrayList<DetalleOrden> detalleOrden = new ArrayList<>();

        orderID = 10248;

        cn = ConexionBD.getConnection();
        query = "SELECT DISTINCT OD.ProductID, P.ProductName, OD.UnitPrice, "
                + "OD.Quantity FROM Orders O, Products P, [Order Details] OD "
                + "WHERE P.ProductID = OD.ProductID AND OD.OrderID = ?;";
        // Obtención del detalle de la orden de compra
        try {
            ps = cn.prepareStatement(query);
            ps.setInt(1, orderID);
            rs = ps.executeQuery();
            while (rs.next()) {
                detalleOrden.add(new DetalleOrden(rs.getInt(1), rs.getString(2),
                        rs.getDouble(3), rs.getInt(4)));
            }
            for (int i = 0; i < detalleOrden.size(); i++) {
                System.out.println(detalleOrden.get(i));
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            System.err.println("ERROR (PruebaConexion): " + e.toString());
        }

    }
}

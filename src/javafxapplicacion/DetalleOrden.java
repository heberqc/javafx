package javafxapplicacion;

public class DetalleOrden {

    private int codigo;
    private String nombre;
    private Double precioUnitario;
    private int cantidad;
    private Double subtotal;

    public DetalleOrden() {
        this.codigo = 0;
        this.nombre = "";
        this.precioUnitario = 0.0;
        this.cantidad = 0;
        this.subtotal = 0.0;
    }

    public DetalleOrden(int codigo, String nombre, Double precioUnitario, int cantidad) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.precioUnitario = precioUnitario;
        this.cantidad = cantidad;
        this.subtotal = cantidad * precioUnitario;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(Double precioUnitario) {
        this.precioUnitario = precioUnitario;
        this.subtotal = getSubtotal();
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Double getSubtotal() {
        if (subtotal == 0.0) {
            subtotal = cantidad * precioUnitario;
        }
        return subtotal;
    }

    @Override
    public String toString() {
        return codigo + "\t" + nombre + "\t" + precioUnitario + "\t" + cantidad + "\t" + subtotal;
    }

}

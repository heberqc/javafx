package javafxapplicacion;

public class Orden {

    private int orderID;
    private String clienteID;
    private String fecha;
    private int empleadoID;
    private String envioNombre;
    private String envioDireccion;
    private String envioCiudad;
    private String envioRegion;
    private String fechaRequerida;
    private String envioPais;
    private String envioPostal;

    public Orden() {
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getClienteID() {
        return clienteID;
    }

    public void setClienteID(String clienteID) {
        this.clienteID = clienteID;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getEmpleadoID() {
        return empleadoID;
    }

    public void setEmpleadoID(int empleadoID) {
        this.empleadoID = empleadoID;
    }

    public String getEnvioNombre() {
        return envioNombre;
    }

    public void setEnvioNombre(String envioNombre) {
        this.envioNombre = envioNombre;
    }

    public String getEnvioDireccion() {
        return envioDireccion;
    }

    public void setEnvioDireccion(String envioDireccion) {
        this.envioDireccion = envioDireccion;
    }

    public String getEnvioCiudad() {
        return envioCiudad;
    }

    public void setEnvioCiudad(String envioCiudad) {
        this.envioCiudad = envioCiudad;
    }

    public String getEnvioRegion() {
        return envioRegion;
    }

    public void setEnvioRegion(String envioRegion) {
        this.envioRegion = envioRegion;
    }

    public String getFechaRequerida() {
        return fechaRequerida;
    }

    public void setFechaRequerida(String fechaRequerida) {
        this.fechaRequerida = fechaRequerida;
    }

    public String getEnvioPais() {
        return envioPais;
    }

    public void setEnvioPais(String envioPais) {
        this.envioPais = envioPais;
    }

    public String getEnvioPostal() {
        return envioPostal;
    }

    public void setEnvioPostal(String envioPostal) {
        this.envioPostal = envioPostal;
    }
}

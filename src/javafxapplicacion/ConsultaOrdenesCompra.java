package javafxapplicacion;

import baseDatos.ConexionBD;
import java.sql.SQLException;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ConsultaOrdenesCompra extends Application {

    Scene scene;
    StackPane stackPane;
    VBox vbox_principal, vbox_lblOrden, vbox_txtOrden, vbox_lblDatosEnvio1,
            vbox_lblDatosEnvio2, vbox_txtDatosEnvio1, vbox_txtDatosEnvio2,
            vbox_lblTotales, vbox_txtTotales;
    HBox hbox_datosEnvio, hbox_datosOrdenEnvio, hbox_search, hbox_totales;
    Image image;
    TitledPane tp_datosEnvio;
    Label lbl_ordenCompra, lbl_cliente, lbl_fecha, lbl_vendedor, lbl_nombre,
            lbl_direccion, lbl_ciudad, lbl_region, lbl_fechaEnvio, lbl_pais,
            lbl_codPostal, lbl_valorVenta, lbl_IGV, lbl_total, lbl_search;
    TextField txt_ordenCompra, txt_cliente, txt_fecha, txt_vendedor,
            txt_nombre, txt_direccion, txt_ciudad, txt_region,
            txt_fechaEnvio, txt_pais, txt_codPostal, txt_valorVenta,
            txt_IGV, txt_total;
    TableView<DetalleOrden> tabla;
    TableColumn<DetalleOrden, Integer> col_codigo, col_cantidad;
    TableColumn<DetalleOrden, String> col_nombre;
    TableColumn<DetalleOrden, Double> col_precioUnitario, col_subtotal;
    Button btn_cerrar;

    Orden orden;
    int orderID;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) {

        // Sección de consulta de orden de compra
        lbl_ordenCompra = new Label("Orden de compra:");
        txt_ordenCompra = new TextField();
        image = new Image(getClass().getResourceAsStream("/res/ic_search.png"));
        lbl_search = new Label("", new ImageView(image));
        hbox_search = new HBox(5);
        hbox_search.getChildren()
                .addAll(lbl_ordenCompra, txt_ordenCompra, lbl_search);

        // Sección de datos de la compra
        // Columna de etiquetas
        lbl_cliente = new Label("Cliente\t:");
        lbl_fecha = new Label("Fecha\t:");
        lbl_vendedor = new Label("Vendedor\t:");
        vbox_lblOrden = new VBox(12);
        vbox_lblOrden
                .getChildren()
                .addAll(lbl_cliente, lbl_fecha, lbl_vendedor);
        // Columna de campos de texto
        txt_cliente = new TextField();
        txt_fecha = new TextField();
        txt_vendedor = new TextField();
        vbox_txtOrden = new VBox(2);
        vbox_txtOrden
                .getChildren()
                .addAll(txt_cliente, txt_fecha, txt_vendedor);

        // Sección de los datos de envío
        // Columna de etiquetas
        lbl_nombre = new Label("Nombre\t:");
        lbl_direccion = new Label("Dirección\t:");
        lbl_ciudad = new Label("Ciudad\t:");
        lbl_region = new Label("Región\t:");
        vbox_lblDatosEnvio1 = new VBox(12);
        vbox_lblDatosEnvio1
                .getChildren()
                .addAll(lbl_nombre, lbl_direccion, lbl_ciudad, lbl_region);
        // Columna de campos de texto
        txt_nombre = new TextField();
        txt_direccion = new TextField();
        txt_ciudad = new TextField();
        txt_region = new TextField();
        vbox_txtDatosEnvio1 = new VBox(2);
        vbox_txtDatosEnvio1
                .getChildren()
                .addAll(txt_nombre, txt_direccion, txt_ciudad, txt_region);
        // Columna de etiquetas
        lbl_fechaEnvio = new Label("Fecha\t\t:");
        lbl_pais = new Label("País\t\t\t:");
        lbl_codPostal = new Label("Código postal\t:");
        vbox_lblDatosEnvio2 = new VBox(12);
        vbox_lblDatosEnvio2
                .getChildren()
                .addAll(lbl_fechaEnvio, lbl_pais, lbl_codPostal);
        // Columna de campos de texto
        txt_fechaEnvio = new TextField();
        txt_pais = new TextField();
        txt_codPostal = new TextField();
        vbox_txtDatosEnvio2 = new VBox(2);
        vbox_txtDatosEnvio2
                .getChildren()
                .addAll(txt_fechaEnvio, txt_pais, txt_codPostal);
        // Agregar las columnas a la sección de datos de envío
        hbox_datosEnvio = new HBox(2);
        hbox_datosEnvio
                .getChildren()
                .addAll(vbox_lblDatosEnvio1, vbox_txtDatosEnvio1,
                        vbox_lblDatosEnvio2, vbox_txtDatosEnvio2);
        // Enmarcar la sección de datos de envío en un panel con título
        tp_datosEnvio = new TitledPane();
        tp_datosEnvio.setText("Datos de envío");
        tp_datosEnvio.setContent(hbox_datosEnvio);
        tp_datosEnvio.setMinHeight(140);

        /*
         * Agrupar horizontalmente la columnas de datos de la orden de 
         * compra con los datos del envío
         */
        hbox_datosOrdenEnvio = new HBox(5);
        hbox_datosOrdenEnvio
                .getChildren()
                .addAll(vbox_lblOrden, vbox_txtOrden, tp_datosEnvio);

        // Construcción de la tabla
        // Declaración de las columnas de la tabla
        // Columna de código de producto
        col_codigo = new TableColumn<>("Cod. Prod.");
        col_codigo.setMinWidth(120);
        col_codigo.setCellValueFactory(
                new PropertyValueFactory<DetalleOrden, Integer>("codigo"));
        // Columna del nombre del producto
        col_nombre = new TableColumn<>("Nombre del producto");
        col_nombre.setMinWidth(220);
        col_nombre.setCellValueFactory(
                new PropertyValueFactory<DetalleOrden, String>("nombre"));
        // Columna del precio unitario del producto
        col_precioUnitario = new TableColumn<>("Prec. Unitario");
        col_precioUnitario.setMinWidth(120);
        col_precioUnitario.setCellValueFactory(
                new PropertyValueFactory<DetalleOrden, Double>("precioUnitario"));
        // Columna de cantidades compradas
        col_cantidad = new TableColumn<>("Cantidad");
        col_cantidad.setMinWidth(50);
        col_cantidad.setCellValueFactory(
                new PropertyValueFactory<DetalleOrden, Integer>("cantidad"));
        // Columna de subtotales
        col_subtotal = new TableColumn<>("Sub-total");
        col_subtotal.setMinWidth(135);
        col_subtotal.setCellValueFactory(
                new PropertyValueFactory<DetalleOrden, Double>("subtotal"));
        // Declaración de la tabla
        tabla = new TableView<>();
        tabla.setMinHeight(180);
        // Agregar las columnas a la tabla
        tabla.getColumns()
                .addAll(col_codigo, col_nombre, col_precioUnitario,
                        col_cantidad, col_subtotal);

        // Sección de los totales de la compra
        // Columna de etiquetas de los totales
        lbl_valorVenta = new Label("Valor venta\t:");
        lbl_IGV = new Label("IGV (18%)\t\t:");
        lbl_total = new Label("Total\t\t:");
        vbox_lblTotales = new VBox(12);
        vbox_lblTotales.getChildren()
                .addAll(lbl_valorVenta, lbl_IGV, lbl_total);
        // Columa de los campos de texto de los totales
        txt_valorVenta = new TextField();
        txt_IGV = new TextField();
        txt_total = new TextField();
        vbox_txtTotales = new VBox(2);
        vbox_txtTotales.getChildren()
                .addAll(txt_valorVenta, txt_IGV, txt_total);
        // Agrupar las columnas de la sección de los totales
        hbox_totales = new HBox(5);
        hbox_totales.setAlignment(Pos.CENTER_RIGHT);
        hbox_totales.getChildren().addAll(vbox_lblTotales, vbox_txtTotales);

        // Botón de cerrar la aplicación
        btn_cerrar = new Button("Cerrar");
        btn_cerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ConexionBD.closeConnection();
                primaryStage.close();
            }
        });

        // Botón para llenar
        txt_ordenCompra.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                orderID = Integer.parseInt(txt_ordenCompra.getText());
                orden = new Orden();
                try {
                    // Mostrar los datos obtenidos de la consulta
                    tabla.setItems(ObtenerDatos.getDetalleOrden(orderID));
                    orden = ObtenerDatos.getOrden(orderID);
                    txt_cliente.setText(orden.getClienteID());
                    txt_fecha.setText(orden.getFecha());
                    txt_vendedor.setText("" + orden.getEmpleadoID());
                    txt_nombre.setText(orden.getEnvioNombre());
                    txt_direccion.setText(orden.getEnvioDireccion());
                    txt_ciudad.setText(orden.getEnvioCiudad());
                    txt_region.setText(orden.getEnvioRegion());
                    txt_fechaEnvio.setText(orden.getFechaRequerida());
                    txt_pais.setText(orden.getEnvioPais());
                    txt_codPostal.setText(orden.getEnvioPostal());
                    // Calcular y mostrar los valores totales
                    txt_valorVenta.setText(round(ObtenerDatos.getValorVenta(), 3));
                    txt_IGV.setText(round(0.18 * ObtenerDatos.getValorVenta(), 3));
                    txt_total.setText(round(1.18 * ObtenerDatos.getValorVenta(), 3));
                } catch (SQLException e) {
                    System.out.println("No se pudo obtener datos: " + e.toString());
                }
            }
        });

        // Agrupar las secciones de la aplicación
        vbox_principal = new VBox(10);
        vbox_principal.setAlignment(Pos.CENTER);
        vbox_principal
                .getChildren()
                .addAll(hbox_search, hbox_datosOrdenEnvio, tabla,
                        hbox_totales, btn_cerrar);

        // Mostrar el conjunto de las secciones en la ventana
        stackPane = new StackPane();
        stackPane.setPadding(new Insets(10));
        stackPane.getChildren().add(vbox_principal);
        scene = new Scene(stackPane, 700, 500);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Consulta de órdenes de compra");
        primaryStage.setResizable(false);
        txt_ordenCompra.requestFocus();
        primaryStage.show();
    }

    // Método para redondear un decimal y mostrarlo como String
    public static String round(double valor, int canDecimales) {
        long factor = (long) Math.pow(10, canDecimales);
        valor = valor * factor;
        long tmp = Math.round(valor);
        return String.valueOf((double) tmp / factor);
    }

}

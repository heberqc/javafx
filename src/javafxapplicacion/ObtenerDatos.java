package javafxapplicacion;

import baseDatos.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ObtenerDatos {

    static Connection cn;
    static PreparedStatement ps;
    static ResultSet rs;
    static String query;
    static boolean existe;
    static Double valorVenta;

    protected static Orden getOrden(int orderID) throws SQLException {
        Orden ordenCompra = new Orden();
        existe = false;
        query = "SELECT OrderID, C.CompanyName, CONVERT(VARCHAR(11), "
                + "OrderDate, 103) AS \"OrderDate\", EmployeeID, ShipName, "
                + "ShipAddress, ShipCity, ShipRegion, "
                + "CONVERT(VARCHAR(11), RequiredDate, 103) AS \"RequiredDate\", "
                + "ShipCountry, ShipPostalCode FROM Orders O, Customers C "
                + "WHERE O.CustomerID=C.CustomerID AND OrderID = ?;";
        try {
            ps = cn.prepareCall(query);
            ps.setInt(1, orderID);
            rs = ps.executeQuery();
            while (rs.next()) {
                if (orderID == rs.getInt(1)) {
                    existe = true;
                    ordenCompra.setClienteID(rs.getString(2));
                    ordenCompra.setFecha(rs.getString(3));
                    ordenCompra.setEmpleadoID(rs.getInt(4));
                    ordenCompra.setEnvioNombre(rs.getString(5));
                    ordenCompra.setEnvioDireccion(rs.getString(6));
                    ordenCompra.setEnvioCiudad(rs.getString(7));
                    ordenCompra.setEnvioRegion(rs.getString(8));
                    ordenCompra.setFechaRequerida(rs.getString(9));
                    ordenCompra.setEnvioPais(rs.getString(10));
                    ordenCompra.setEnvioPostal(rs.getString(11));
                    break;
                }
            }
        } catch (SQLException e) {
            System.err.println("ERROR: " + e.toString());
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException e) {
                System.err.println("ERROR: " + e.toString());
            }
        }
        return ordenCompra;
    }

    protected static ObservableList<DetalleOrden> getDetalleOrden(int orderID)
            throws SQLException {
        // Atributos
        ObservableList<DetalleOrden> detalleOrden
                = FXCollections.observableArrayList();
        cn = ConexionBD.getConnection();
        query = "SELECT DISTINCT OD.ProductID, P.ProductName, OD.UnitPrice, "
                + "OD.Quantity FROM Orders O, Products P, [Order Details] OD "
                + "WHERE P.ProductID = OD.ProductID AND OD.OrderID = ?;";
        valorVenta = 0.0;
        // Obtención del detalle de la orden de compra
        try {
            ps = cn.prepareStatement(query);
            ps.setInt(1, orderID);
            rs = ps.executeQuery();
            while (rs.next()) {
                detalleOrden.add(new DetalleOrden(rs.getInt(1), rs.getString(2),
                        rs.getDouble(3), rs.getInt(4)));
            }
            for (int i = 0; i < detalleOrden.size(); i++) {
                valorVenta = valorVenta + detalleOrden.get(i).getSubtotal();
            }
        } catch (SQLException e) {
            System.err.println("ERROR: " + e.toString());
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException e) {
                System.err.println("ERROR: " + e.toString());
            }
        }
        return detalleOrden;
    }
    
    protected static Double getValorVenta() {
        return valorVenta;
    }
    
}
